var _ = require('underscore'),
	cfg = require('./config.json');


// set all the names in the config to lowercase
var authorizedUsers = _.map(cfg.authorizedUsers, function(name) {
	return name.toLowerCase();
});

var xmpp,
	conn,
	messages = [];

function request_roster() {

	console.log('Requesting Roster');

	var roster_elem = new xmpp.Element(
			'iq',
			{
				from: conn.jid,
				type: 'get',
				id: 'roster'
			}
		).c('query', { xmlns: 'jabber:iq:roster' });

	conn.send(roster_elem);

}

function sendMessage(to, message) {

	var newMessage = new xmpp.Element(
			'message',
			{ 
				to: to,
				type: 'chat'
			}
		).c('body').t(message);

	conn.send(newMessage);
}

module.exports.init = function(_xmpp, _conn) {
	xmpp = _xmpp;
	conn = _conn;
}

module.exports.handleMessage = function(from, body) {

	var uid = from.split('@')[0];

	console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
	console.log('Message from:' + from);
	console.log(body);
	console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

	if(_.contains(cfg.authorizedUsers, uid.toLowerCase())) {

		console.log('Ping request from: ' + uid);
		conn.send(from, 'echo: ' + body);

		messages.push({
			from: from,
			body: [
				'',
				body,
				'',
				'~~~~ Sent by ' + uid + ' ~~~~'
			].join('\n')
		});

		request_roster();
	}
	else {
		console.log('Bad message from ' + from);
	}

}

module.exports.onRosterLoad = function(children) {

	console.log('Roster load');

	// Loop through each online user
	_.each(children, function(child) {

		var targetUserID = child.attrs.jid;
		
		console.log('Sending messages to ' + targetUserID);

		// Loop through each message
		_.each(messages, function(message) {

			// Send the user the message
			console.log(message)
			sendMessage(targetUserID, message.body);

		});

	});

	// Clear message queue
	messages = [];

}


module.exports.setStatus = function(status) {

	//console.log('Setting Status');

	conn.send(
		new xmpp.Element('presence', { })
			.c('show').t('chat').up()
			.c('status').t(status)
		);

}