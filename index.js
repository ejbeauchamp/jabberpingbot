
// Libs
var xmpp = require('node-xmpp'),
	_ = require('underscore');

// Vars
var cfg = require('./config.json'),
	messenger = require('./messenger.js');


// Establish Connection
var conn = new xmpp.Client({
		jid: cfg.user.name.toLowerCase() + '@' + cfg.server.url,
		password: cfg.user.password,
		host: cfg.server.url,
		port: cfg.server.port
	});

// Initiate the messenger
messenger.init(xmpp, conn);

// Handle messages
// conn.on('chat', function(from, message) {
// 	console.log(message);
// });

// Verify login success
conn.on('online', function(data) {
	console.log('Connection Established');
	messenger.setStatus('Accepting Pings');
	// messenger.handleMessage('test1', 'hello world');
});

// Send errors to the log
conn.on('error', function(err) {
	console.error(err);
});

conn.on('stanza', function(response) {

	console.log(response.nodeName);

	if (response.attrs.id === 'roster') {
		messenger.onRosterLoad(response.children[0].children);
	}
	else if (response.is('message') /* && response.attrs.type !== 'error' */) {
		_.each(response.children, function(child) {
			if(child.nodeName == 'body') {
				messenger.handleMessage(response.attrs.from, child.children.join('\n'));
			}
		});
	}

});

